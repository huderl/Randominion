# Randominion

A React app to draw cards for the Dominion board game in French. Deployed at https://huderl.gitlab.io/Randominion/.

## Features

Clicking on the button _Tirer 10 nouvelles cartes_ draws a random set of 10 cards, sorted by cost for easy set-up.

Check/uncheck boxes to include/exclude the sets of the supported boxes:

- Original Dominion 1st edition (_Dominion classique_)
- Intrigue 1st edition
- Prosperity (_Prospérité_)
- Hinterlands (_Arrière pays_)
- Dark Ages (_Âge des ténèbres_)
