import { ReactElement } from "react";
import { Card } from "./models";
import RULES from "./rules";
import styles from "./RulesDisplay.module.css";

interface Props {
  cards: Card[];
}

function RulesDisplay(props: Props): ReactElement {
  const { cards } = props;
  const rules = RULES.filter((r) => r.condition(cards));

  if (rules.length === 0) {
    return <></>;
  }

  return (
    <div className={styles.container}>
      <h3>Règles additonnelles</h3>
      <ul>
        {rules.map((rule) => (
          <li key={rule.name}>{rule.content}</li>
        ))}
      </ul>
    </div>
  );
}

export default RulesDisplay;
