export enum Extension {
  ArrierePays = "AP",
  DominionClassique = "DR",
  Intrigue = "DI",
  Prosperite = "PR",
  AgeDesTenebres = "DA",
}

export const EXTENSION_LABELS: Record<Extension, string> = {
  [Extension.ArrierePays]: "Arrière pays",
  [Extension.DominionClassique]: "Dominion classique",
  [Extension.Intrigue]: "Intrigue",
  [Extension.Prosperite]: "Prospérité",
  [Extension.AgeDesTenebres]: "Âge des ténèbres",
};

export const EXTENSIONS: Extension[] = Object.values(Extension);

export enum CardType {
  Action = "Action",
  Attaque = "Attaque",
  Chevalier = "Chevalier",
  Pillard = "Pillard",
  Reaction = "Réaction",
  Tresor = "Trésor",
  Victoire = "Victoire",
}

export interface Rule {
  name: string;
  condition: (cards: Card[]) => boolean;
  extension: Extension;
  content: string;
}

export interface Card {
  name: string;
  cost: number;
  type: CardType[];
  extension: Extension;
}
