import { useState } from "react";
import { Card } from "./models";
import CardsDisplay from "./CardsDisplay";
import styles from "./App.module.css";
import Controls from "./Controls";
import RulesDisplay from "./RulesDisplay";

function App() {
  const [drawnCards, setDrawnCards] = useState<Card[]>([]);

  return (
    <div className={styles.app}>
      <Controls setCards={(cards: Card[]) => setDrawnCards(cards)} />
        <CardsDisplay cards={drawnCards} />
        <RulesDisplay cards={drawnCards} />
    </div>
  );
}

export default App;
