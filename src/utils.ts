import { sampleSize } from "lodash-es";
import CARDS from "./cards";
import { Card, Extension } from "./models";

export function sortByCost(card1: Card, card2: Card) {
  return card1.cost - card2.cost;
}

export function drawTenUniqueCards(extensions: Extension[]): Card[] {
  const cardsToDraw = extensions.reduce<Card[]>(
    (acc, extension) =>
      (acc = [
        ...acc,
        ...CARDS[extension].map((c) => {
          return { ...c, extension };
        }),
      ]),
    []
  );

  if (cardsToDraw.length < 10) {
    return [];
  }

  return sampleSize(cardsToDraw, 10).sort(sortByCost);
}
