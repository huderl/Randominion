import React, { useState } from "react";
import styles from "./Controls.module.css";
import { Card, Extension, EXTENSIONS, EXTENSION_LABELS } from "./models";
import { drawTenUniqueCards } from "./utils";

interface Props {
  setCards: (cards: Card[]) => void;
}

function Controls(props: Props): JSX.Element {
  const { setCards } = props;

  const [extensionsToUse, setExtensions] = useState<Extension[]>(EXTENSIONS);

  return (
    <div className={styles.container}>
      <h1>Randominion</h1>
      <div>
        <button
          className={styles.drawButton}
          onClick={() => setCards(drawTenUniqueCards(extensionsToUse))}
        >
          Tirer 10 nouvelles cartes
        </button>
      </div>
      <h2>Extensions à tirer</h2>
      <div className={styles.checkboxContainer}>
        {EXTENSIONS.map((ext) => {
          return (
            <label key={ext} className={styles.extLabel}>
              <input
                className={styles.extCheckbox}
                type="checkbox"
                checked={extensionsToUse.includes(ext)}
                value={ext}
                onChange={() => {
                  if (extensionsToUse.includes(ext)) {
                    setExtensions(
                      extensionsToUse.filter((extToUse) => extToUse !== ext)
                    );
                  } else {
                    setExtensions([...extensionsToUse, ext]);
                  }
                }}
              />
              {EXTENSION_LABELS[ext]}
            </label>
          );
        })}
      </div>
    </div>
  );
}

export default Controls;
