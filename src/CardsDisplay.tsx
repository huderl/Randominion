import { Card } from "./models";
import styles from "./CardsDisplay.module.css";

interface Props {
  cards: Card[];
}

function CardsDisplay(props: Props) {
  const { cards } = props;
  return (
    <div className={styles.container}>
      {cards.map((card, i) => {
        return (
          <>
                {i === 5 && <div className={styles.separator} />}
                <div
                  className={styles.card}
                  key={card.name}
                  data-extension={card.extension}
                >
                  <div className={styles.name}>{card.name}</div>
                  <div className={styles.cost}>{card.cost}</div>
                </div>
          </>
        );
      })}
    </div>
  );
}

export default CardsDisplay;
