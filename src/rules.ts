import { sample } from "lodash-es";
import { CardType, Extension, Rule } from "./models";

const RULES: Rule[] = [
  {
    name: "Butin",
    content: "Utilisez la pile Butin.",
    extension: Extension.AgeDesTenebres,
    condition: (cards) =>
      cards.some(
        (card) =>
          card.name === "Camp de bandits" ||
          card.name === "Maraudeur" ||
          card.name === "Pillage"
      ),
  },
  {
    name: "Fou",
    content: "Utilisez la pile Fou.",
    extension: Extension.AgeDesTenebres,
    condition: (cards) => cards.some((card) => card.name === "Ermite"),
  },
  {
    name: "Mercenaire",
    content: "Utilisez la pile Mercenaire.",
    extension: Extension.AgeDesTenebres,
    condition: (cards) => cards.some((card) => card.name === "Orphelin"),
  },
  {
    name: "Refuge",
    content: "Remplacez les cartes Domaine de départ par des cartes Refuge.",
    extension: Extension.AgeDesTenebres,
    condition: (cards) => sample(cards)?.extension === Extension.AgeDesTenebres,
  },
  {
    name: "Ruines",
    content: "Utilisez la pile Ruines",
    extension: Extension.AgeDesTenebres,
    condition: (cards) =>
      cards.some((card) => card.type.includes(CardType.Pillard)),
  },
  {
    name: "Prospérité",
    content: "Utilisez les piles Colonie et Platine.",
    extension: Extension.Prosperite,
    condition: (cards) => sample(cards)?.extension === Extension.Prosperite,
  },
  {
    name: "Jetons victoire",
    content:
      "Distribuez un plateau à chaque joueur pour y placer les jetons Victoire.",
    extension: Extension.Prosperite,
    condition: (cards) =>
      cards.some(
        (card) =>
          card.name === "Evêque" ||
          card.name === "Fiers-à-bras" ||
          card.name === "Monument"
      ),
  },
  {
    name: "Route commerciale",
    content:
      "Placez un jeton Pièce sur chaque carte Victoire et placez le plateau de la Route commerciale.",
    extension: Extension.Prosperite,
    condition: (cards) =>
      cards.some((card) => card.name === "Route commerciale"),
  },
];

export default RULES;
